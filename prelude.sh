# The compose environment file
ENV_FILE=.env

# Simple logging
function log() {
    echo "##[INFO ]: ${@}"
}
function warn() {
    echo -ne "##[\033[0;31mWARN\033[0m ]: "
    echo "${@}"
}
function error() {
    echo -ne "##[\033[0;31mERROR\033[0m]: "
    echo "${@}"
}

# Check whether some files satisfy a test
function _check_files(){
    local missing=0
    local test=$1
    shift 1
    while [[ -n "$1" ]]; do
        if test ! $test "$1"; then
            warn "Something wrong with ${1}."
            missing=$((missing + 1))
        fi
        shift 1
    done
    return $missing
}
function check_files(){ _check_files -f $@ ; }
function check_execs(){ _check_files -x $@ ; }

# Check if build dir is there
if [[ ! -d build || \
    $(check_files $ENV_FILE docker-compose.yml docker-compose.setup.yml) -ne 0 ]]
then
    error "Cannot find what we need. Are we in the right place?"
    exit 1
fi

# Reads keys out of the ENV_FILE
source $ENV_FILE

# Check for project name
PROJECT=$COMPOSE_PROJECT_NAME
if [[ -z $PROJECT ]]; then
    error "Could not find project name"
    exit 1
else
    log "Use project \"${PROJECT}\""
fi

# Check whether a subdir is a valid plugin and abort if not
function check_plugin() {
    pushd "$1"
    check_files docker-compose.yml
    check_execs configure.sh build.sh start.sh
    if [[ $? -ne 0 ]]; then
        error "It seems ${1} is not a valid plugin. Aborting."
        exit 1
    fi
    popd
}

function do_plugins() {
    [[ -n "$1" ]] || exit 1
    for p in plugins/*; do
        if [[ -d "$p" ]]; then
            log "Check plugin ${p}..."
            check_plugin "$p"
            log "${1} plugin ${p}..."
            pushd "$p"
            ./${1}.sh
            popd
        fi
    done
}




