#!/bin/bash

source prelude.sh

while [[ -n $1 ]]; do
    ARGS="${ARGS} --profile $1"
    shift 1
done

log "Stopping with arguments ${ARGS:---}"

docker compose $ARGS stop

do_plugins stop

