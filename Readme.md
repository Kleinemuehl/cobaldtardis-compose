## Dependencies
- `apptainer / singularity`
- `docker`
- `docker-compose (v2)`
- `jq`
- `python3` and `jinja2`

## Setup
How to set everything up from scratch.

### Summary
In the project directory, run
```
0 $ vim .env
1 $ ./configure.sh
2 $ ./build.sh
3 $ docker compose [--profile pool] up
```

#### 0: Edit `.env`
This file contains all variables needed to adapt the setup to a given site.

#### 1: `configure.sh`
Will try to install an SSH key on the slurm submit node. Should this fail a warning will be issued and you have to do it by hand.
You will find the key in `build/login_key.pub`.

Will also copy auth tokens to telegraf / grafana config files.

**Warning**: Keep in mind to delete secrets before pushing to git!

#### 2: `build.sh`
Will build all required images.

#### 3: compose up
The HTCondor central manager and connection broker are only started if `--profile pool` is used.
This makes it more convenient to set them up on separate machines.

## Notes
If you change settings in `.env`, it is normally required to apply these to the configuration files using `python3 render_templates.py -f` and rebuild the containers `./build.sh`.