#!/bin/bash
set -e -o pipefail  #Crash early!

source prelude.sh

### Bring up influx setup, wait until it is up (tokens written) and shut it down again
#log "Prepare Influx..."
#docker compose -f docker-compose.yml -f docker-compose.setup.yml up --detach influx
#while ! curl http://localhost:$INFLUX_PUB_PORT > /dev/null 2>&1; do
#    sleep 0.5
#done
#docker compose stop influx

### Render all templates
log "Render all templates..."
python3 render_templates.py -f

### Prepare ssh keys
pushd build/custom
# figure out host and port
if grep --quiet ':' <<< $SLURM_SUB_HOST; then
    HOST=${SLURM_SUB_HOST%:*}
    PORT=${SLURM_SUB_HOST#*:}
else
    HOST=${SLURM_SUB_HOST}
    PORT=22
fi
# Used to login to slurm-submit
if [[ ! -f login_key.pub || ! -f cobald/login_key ]]; then
    # generate key
    log "Generate new ssh keys"
    if ssh-keygen -qN '' -C 'Cobald login' -t ed25519 -f secrets/login_key; then
        # install key
        log "Install them for ${EXTERNAL_USER}@${HOST}:${PORT}"
        ssh-copy-id -f -i secrets/login_key -p $PORT $EXTERNAL_USER@$HOST || \
            warn "Could not copy ssh key. Do it manually!"
    else
        warn "Skipping key install!"
    fi
fi

# Hostkey for slurm-submit
log "Collect fingerprints from ssh host"
ssh-keyscan -p $PORT $HOST > known_hosts
popd

### Run Plugin configures
log "Configure plugins..."
do_plugins configure







