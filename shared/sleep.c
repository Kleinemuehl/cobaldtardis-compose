#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    int t = 60;
    long m = 128;

    if (argc > 2)
        t = atoi(argv[2]);
    if (argc > 1)
        m = atol(argv[1]);

    printf("Allocating %dMiB...\n", m);
    m *= 1024 * 1024;
    char *p = malloc(m);
    // Write stuff in case of lazy allocation...
    for(long i = 0; i < m; i++)
        p[i] = i;

    printf("Sleeping for %ds...\n", t);
    sleep(t);

    free(p);
}
