#!/bin/bash
set -e -o pipefail  #Crash early!

source prelude.sh

# Parse options
ECHO=''
declare -a FORCE
OPTIND=1
while getopts "cnf:" OPT; do
    case "${OPT}" in
        c)  ./configure.sh
            ;;
        n)  ECHO='echo'
            ;;
        f)  FORCE+=(${OPTARG})
            ;;
    esac
done
shift $((OPTIND-1))

################################################################################
################################################################################
# Build docker images
log "Build docker images..."
$ECHO docker compose --profile pool --profile debug build --progress plain

# Build the drone image
SIF_TIME=$(stat --format=%Y shared/cobaldtardis/drone.sif 2> /dev/null || echo 0)
DCK_TIME=$(docker image inspect ${PROJECT}-htcondor_cm | jq '.[].Created' | tr --delete '"' | date --file - '+%s' 2> /dev/null || echo 0)
DEF_TIME=$(stat --format=%Y build/drones/drone.def)
if [[ $DEF_TIME -gt $SIF_TIME || $DCK_TIME -gt $SIF_TIME || " ${FORCE[*]} " =~ " drones " ]]; then
    log "Building apptainer image"
    $ECHO docker compose up apptainer
fi

if [[ -a $SHARED_DIR_MNT && ! -d $SHARED_DIR_MNT ]]; then
    warn "$SHARED_DIR_MNT is not a directory. Ignoring"
else
    if [[ ! -d $SHARED_DIR_MNT ]]; then
        log "Create utility folder: ${SHARED_DIR_MNT}"
        mkdir $SHARED_DIR_MNT
    fi
    log "Prepare utility folder: ${SHARED_DIR_MNT}"
    $ECHO chmod 777 $SHARED_DIR_MNT
    $ECHO cp ./shared/sleep.{c,sub} $SHARED_DIR_MNT/
fi

function make_shared_dir() {
    $ECHO mkdir -p $1/cobaldtardis
    $ECHO chmod -R 777 $1/cobaldtardis
    $ECHO cp ./shared/cobaldtardis/drone.sif $1/cobaldtardis/
    $ECHO cp ./shared/cobaldtardis/drone_job $1/cobaldtardis/
    $ECHO cp ./shared/cobaldtardis/drone_run $1/cobaldtardis/
}

chmod 755 ./shared/cobaldtardis/drone_*
if [[ -d $SHARED_DIR_SLURM ]]; then
    log "Prepare shared folder: ${SHARED_DIR_SLURM}"
    make_shared_dir $SHARED_DIR_SLURM
else
    DIR=$(mktemp --directory cobaldtardis.XXXX)
    warn "Shared folder not found. Make sure the contents of ${DIR} end up in ${SHARED_DIR_SLURM}."
    make_shared_dir $DIR
fi

### Run Plugin builds
log "Build plugins..."
$ECHO do_plugins build


