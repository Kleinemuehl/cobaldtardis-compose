#!/bin/bash
set -e -o pipefail  #Crash early!

source prelude.sh

while [[ -n $1 ]]; do
    ARGS="${ARGS} --profile $1"
    shift 1
done

log "Starting with arguments ${ARGS:---}"

docker compose $ARGS up --detach

do_plugins start

