#!/bin/bash

# Create a new bucket
influx bucket create --name drone_usage --retention 90d

# Create Tokens
influx auth create --description telegraf_write --org COBalD_TARDIS --write-buckets > /dev/null
influx auth create --description grafana_read --org COBalD_TARDIS --read-buckets > /dev/null
# Write them to a file outside the container
influx auth list --hide-headers --json > /shared/influx_tokens.json

