#!/bin/bash

source prelude.sh


docker compose --profile pool --profile debug down --volumes
if [[ $1 == '-a' ]]; then
    docker image rm $(docker image ls --quiet \
        --filter "label=com.docker.compose.project=${PROJECT}")
fi
docker system prune -f

rm build/custom/secrets/login_key.pub
rm build/custom/secrets/login_key

do_plugins clean
